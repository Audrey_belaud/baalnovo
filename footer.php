<div class="footer">

	<div class="left">
	Copyright 2019, Design & Code: <a href="http://www.mariusjopen.world" target="_blank">Marius Jopen</a>
	</div>

	<div class="middle back">
		<?php the_field('zuruck', 'option'); ?>
	</div>

	<div class="right menu-button">
		<?php the_field('menu', 'option'); ?>
	</div>


</div>

</div>

<?php wp_footer() ?>
</body>
</html>
