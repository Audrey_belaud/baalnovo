<!-- START PRESSE -->
<?php
if( have_rows($presse) ):
?>

	<div class="presse">


		<?php
		while ( have_rows($presse) ) : the_row();
		?>

		<div class="download-datei">

				<div class="post">
					<div class="presse-datei">

						<?php
						$file = get_sub_field('datei');	
						$name = get_sub_field('datei_name');			
						?>

						<a href="<?php echo $file ?>" target="_blank">
						<?php echo $name ?>
						</a>
					</div>
				</div>

			</div>

		<?php
		endwhile;
		?>

	</div>

<?php
else :
endif;
?>

<!-- END PRESSE -->
