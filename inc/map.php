<!-- START MAP -->
<?php
if( have_rows($event) ):

	if ($date_url != "") :
	?>

		<div class="map">



			<?php
			while( have_rows($event) ): the_row();

			$date_time = get_sub_field('date', false, false) . get_sub_field('zeit', false, false);
			$date_time_mod = str_replace(':', '', $date_time);

			if ($date_url == $date_time_mod) {
            ?>
            
            <?php
            $post_object = get_sub_field('lokation');
            if( $post_object ) :

                $post = $post_object;
                setup_postdata($post);

                $strasse = get_field('strasse');
                $hausnummer = get_field('hausnummer');
                $postleitzahl = get_field('postleitzahl');
                $stadt = get_field('stadt');
                $land = get_field('land');
                $title = get_the_title();
                ?>

                        <?php
                        // echo $title;
                        // echo $strasse;
                        // echo $hausnummer;
                        // echo $postleitzahl;
                        // echo $stadt;
                        // echo $land;
                        ?>

                <?php
                    wp_reset_postdata( $post );
            endif;
            ?>  

            <?php
			};

        endwhile;
        ?>

    
    </div>

<?php
endif;

endif;
?>

<!-- END MAP -->
