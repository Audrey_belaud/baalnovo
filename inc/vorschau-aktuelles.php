<!-- START VORSCHAU AKTUELLES -->
<div class="vorschau-aktuelles">

	<?php
	query_posts(array(
		'post_type' => 'aktuelles',
		'posts_per_page' => 4,
	) );

	while (have_posts()) : the_post();
	?>

		<div class="vorschau-aktuell">

			<?php
			$image = get_field('vorschau_bild');
			include(locate_template('inc/image-vorschau.php'));

			include(locate_template('inc/title-link.php'));

			?>

		</div>

	<?php
	endwhile;
	?>
</div>
<!-- END VORSCHAU AKTUELLES -->
