<!-- START STUECKE FILTER -->
<div class="filter">

	<div class="filter-item filter-all">
		<?php the_field('filter_alle', 'option'); ?>
	</div>

	<div class="filter-item filter-get filter-aktuell">
		<?php the_field('filter_aktuell', 'option'); ?>
	</div>

	<div class="filter-item filter-get filter-archiv">
		<?php the_field('filter_archiv', 'option'); ?>
	</div>

</div>

<div class="filter">

	<?php
	$terms = get_terms( 'stuecke-category' );

	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		foreach ( $terms as $term ) {
		?>

		<div data-id="<?php echo $term->slug ?>" class="filter-item filter-get-id">
			<?php echo $term->name; ?>
		</div>

		<?php
		}
	}
	?>

</div>


<div class="vorschau-stuecke">
	<?php

	query_posts(array(
		'post_type' => 'stuecke',
		'orderby' => 'title',
		'order'   => 'ASC'
	) );

	while (have_posts()) : the_post();

	$terms = get_the_terms( $post->ID, 'stuecke-category' );
	$active = get_field('archiv');

	if( $active == 1 ):
		$active_mod = "aktuell";
	endif;

	if( $active == "" ):
		$active_mod = "archiv";
	endif;




	?>

		<div name="<?php the_title(); ?>" class="vorschau-stueck <?php 	foreach( $terms as $term ) echo $term->slug." hallo "; ?> <?php echo $active_mod ?> ">

			<?php
			include(locate_template('inc/vorschau-stuecke.php'));
			?>

		</div>

	<?php
	endwhile;

	?>
</div>
<!-- END STUECKE FILTER -->
