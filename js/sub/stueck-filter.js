export function stueckfilter($) {

  jQuery(".vorschau-stueck").addClass("filter-active");
  jQuery("body").addClass("filter-archive-active");

  jQuery( ".filter-all" ).click(function() {
    jQuery(".vorschau-stueck").addClass("filter-active");
    jQuery("body").removeClass("filter-aktuell-active");
    jQuery("body").removeClass("filter-archive-active");
    jQuery("body").addClass("filter-all-active");
  });

  // ARCHIV

  jQuery( ".filter-archiv" ).click(function() {
    jQuery(".vorschau-stueck").removeClass("filter-active");
    jQuery(".archiv").addClass("filter-active");

    jQuery("body").removeClass("filter-all-active");
    jQuery("body").removeClass("filter-aktuell-active");
    jQuery("body").addClass("filter-archive-active");
  });

  // AKTUELL

  jQuery( ".filter-aktuell" ).click(function() {
    jQuery(".vorschau-stueck").removeClass("filter-active");
    jQuery(".aktuell").addClass("filter-active");

    jQuery("body").removeClass("filter-all-active");
    jQuery("body").removeClass("filter-archive-active");
    jQuery("body").addClass("filter-aktuell-active");
  });


  // GENRES
  jQuery( ".filter-get-id" ).click(function() {
    var filter = jQuery(this).data("id").toLowerCase().trim();

    jQuery(".vorschau-stueck").removeClass("filter-active");
    jQuery("." + filter).addClass("filter-active");

    if (filter == "jugend-und-erwachsenentheater") {
      jQuery(".sozio-kulturell").addClass("filter-active");
    }


  });



}
