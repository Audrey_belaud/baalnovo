export function randomcolor($) {
  jQuery(".menu-menu-1-container .menu-item-has-children li").addClass("color");

  jQuery(".color").mouseenter(function(e) {
      var randomClass = getRandomClass();

      jQuery(".color").css("background-color", "");
      jQuery(this).css("background-color", randomClass);
  });

  jQuery(".color").mouseleave(function(e) {
      jQuery(".color").css("background-color", "");
  });

}

function getRandomClass() {
    var classes = new Array("#31f288", "#ffc0cb", "#14eefe");
    var randomNumber = Math.floor(Math.random()*3);
    return classes[randomNumber];
}
