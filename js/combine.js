import { Back } from './sub/back.js';
import { dateSorting } from './sub/date-sorting.js';
import { flexslider } from './sub/flexslider.js';
import { menu } from './sub/menu.js';
import { pagename } from './sub/page-name.js';
import { randomcolor } from './sub/random-color.js';
import { scrolltotop } from './sub/scroll-to-top.js';
import { shownavigation } from './sub/show-navigation.js';
import { stueckfilter } from './sub/stueck-filter.js';
import { stuecksort } from './sub/stueck-sort.js';

jQuery(document).ready(function ($) {
    Back($);
    dateSorting($);
    flexslider($);
    menu($);
    pagename($);
    randomcolor($);
    scrolltotop($);
    shownavigation($);
    stueckfilter ($);
    stuecksort($);
});